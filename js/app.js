// Access to the todoList
const todoList = document.querySelector('#todoList');
// Load the eventListeners
eventListeners();

function eventListeners() {
    document.querySelector('#todoInputForm').addEventListener('submit', addTodo);
    document.addEventListener('DOMContentLoaded', loadLocalStorage);
    todoList.addEventListener('click', removeTodo)
}

//Create li tag
function createLiTag(todoContent) {
    // Create li tag
    const li = document.createElement('li');
    // Create remove button
    const removeBtn = document.createElement('a');
    // put X in remove button
    removeBtn.textContent = 'X';
    // Add class to remove Button
    removeBtn.classList.add('removeBtn');
    // Add todoContent to li tag
    li.appendChild(document.createTextNode(todoContent));
    // Add remove Button to li tag
    li.appendChild(removeBtn);
    // Add li tag to list
    todoList.appendChild(li)
}

// add todoItem to list
function addTodo(e) {
    // Get input from user
    const todoValue = document.querySelector('#todoItem').value;
    // Stop the form
    e.preventDefault();
    createLiTag(todoValue);
    this.reset();
    addToLocalStorage(todoValue);
}

// Add the TodoItem to localStorage
function addToLocalStorage(todoValue) {
    // get Todos from LocalStorage
    const todosFromLS = getFromLocalStorage();
    // Push the todoValue to the array
    todosFromLS.push(todoValue);
    // set todos to LocalStorage
    localStorage.setItem('todos', JSON.stringify(todosFromLS));

}

// Get TodoItems from localStorage
function getFromLocalStorage() {
    // Access to Todos in LocalStorage
    let todoFromLS = localStorage.getItem('todos');
    let todos;
    // Check the todoFromLS
    if (todoFromLS === null) {
        todos = [];
    } else {
        todos = JSON.parse(todoFromLS)
    }
    return todos;
}

// Load LocalStorage on Site Loaded
function loadLocalStorage() {
    // Get todos from LocalStorage
    const todosFromLS = getFromLocalStorage();
    // Add the Todos to List
    todosFromLS.map((element) => {
        createLiTag(element);
    })

}

// Remove The TodoItem
function removeTodo(e) {
    const targetValue = e.target.parentElement.textContent;
    if (e.target.classList.contains('removeBtn')) {
        e.target.parentElement.remove();
        removeFromLocalStorage(targetValue);
    }
}
// Remove Todos From localStorage
function removeFromLocalStorage(todo){
    // Access the target TodoItem
    const todoDelete = todo.substring(0, todo.length - 1).trim();
    // Get Todos Array from LocalStorage
    const todoFromLS = getFromLocalStorage();
    todoFromLS.map((element, index) => {
        if(element === todoDelete){
            todoFromLS.splice(index, 1)
        }
    });
    localStorage.setItem('todos', JSON.stringify(todoFromLS));
}

// console.log(getFromLocalStorage())